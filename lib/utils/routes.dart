import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:todolist/anims/animated_card.dart';
import 'package:todolist/constats/app_routes.dart';
import 'package:todolist/constats/app_style.dart';
import 'package:todolist/models/pages_arguments.dart';
import 'package:todolist/pages/404.dart';
import 'package:todolist/pages/add_category.dart';
import 'package:todolist/pages/add_item.dart';
import 'package:todolist/pages/login_view.dart';
import 'package:todolist/pages/main.dart';
import 'package:todolist/pages/todo.dart';

class Routes {
  static Route geneateRoute(RouteSettings settings) {
    //check named route and return page
    switch (settings.name) {
      case RoutePaths.Login:
        return MaterialPageRoute<Widget>(builder: (context) => LoginView());
      case RoutePaths.HomeScreen:
        return MaterialPageRoute<Widget>(builder: (context) => HomeScreen());
      case RoutePaths.AddItem:
        return MaterialPageRoute<Widget>(
            builder: (context) =>
                AddItem(settings.arguments as ItemPageArguments));
      case RoutePaths.Category:
      //return router with card animation
        return CardRoute(
            widget: TodoPage(settings.arguments as MainPageArguments),
            arguments: settings.arguments as MainPageArguments);
      case RoutePaths.AddCategory:
        return CardRoute(
            widget: AddCategory(settings.arguments as MainPageArguments),
            arguments: settings.arguments as MainPageArguments);
      case RoutePaths.EditCategory:
        return MaterialPageRoute<Widget>(
            builder: (context) =>
                AddCategory(settings.arguments as MainPageArguments));
      default:
        return MaterialPageRoute<Widget>(builder: (context) => const Page404());
    }
  }
}

class CardRoute extends PageRouteBuilder<Widget> {
  final Widget widget;
  final MainPageArguments arguments;

  CardRoute({@required this.widget, @required this.arguments})
      : super(
    pageBuilder: (BuildContext context, Animation<double> animation,
        Animation<double> secondaryAnimation) {
      return widget;
      //return AnimationPageInjection(child: widget, animationPage: animation);
    },
    transitionsBuilder: (BuildContext context,
        Animation<double> animation,
        Animation<double> secondaryAnimation,
        Widget child) {
      var curvedAnimation = CurvedAnimation(
        parent: animation,
        curve: Curves.easeOut,
      );

      return CardTransition(
          child: child,
          animation: curvedAnimation,
          cardPosition: arguments.cardPosition);
    },
    transitionDuration: Style.pageDuration,
  );
}
