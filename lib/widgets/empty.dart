import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class EmptyTodo extends StatelessWidget {
  const EmptyTodo({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 400,
      height: 400,
      child: FlareActor.asset(
        AssetFlare(bundle: rootBundle, name: 'assets/animation/teddy_example.flr'),
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: 'idle',
      ),
    );
  }
}
