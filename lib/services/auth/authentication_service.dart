import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn googleSignIn = GoogleSignIn();

Future<String> signInWithGoogle() async {
  // ignore: omit_local_variable_types
  final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
  // ignore: omit_local_variable_types
  final GoogleSignInAuthentication googleSignInAuthentication =
  await googleSignInAccount.authentication;
  // ignore: avoid_print
  print('test');

  // ignore: omit_local_variable_types
  final AuthCredential credential = GoogleAuthProvider.getCredential(
    accessToken: googleSignInAuthentication.accessToken,
    idToken: googleSignInAuthentication.idToken,
  );

  // ignore: omit_local_variable_types
  final AuthResult authResult = await _auth.signInWithCredential(credential);
  // ignore: omit_local_variable_types
  final FirebaseUser user = authResult.user;

  assert(!user.isAnonymous);
  assert(await user.getIdToken() != null);

  // ignore: omit_local_variable_types
  final FirebaseUser currentUser = await _auth.currentUser();
  assert(user.uid == currentUser.uid);
  print(user.displayName);

  return 'signInWithGoogle succeeded: $user';
}

// ignore: avoid_void_async
void signOutGoogle() async{
  await googleSignIn.signOut();

  // ignore: avoid_print
  print('User Sign Out');
}