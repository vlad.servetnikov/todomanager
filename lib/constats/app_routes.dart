class RoutePaths {
  static const String Login = 'login';
  static const String HomeScreen = 'home';
  static const String AddItem = 'add_item';
  static const String Category = 'category';
  static const String AddCategory = 'add_category';
  static const String EditCategory = 'edit_category';
}
