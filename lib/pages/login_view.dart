import 'package:flare_flutter/flare_actor.dart';
import 'package:flare_flutter/provider/asset_flare.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_neumorphic/flutter_neumorphic.dart';
import 'package:todolist/constats/app_routes.dart';
import 'package:todolist/models/test_model.dart';
import 'package:todolist/pages/test_widget.dart';
import 'package:todolist/services/auth/authentication_service.dart';
import '../widgets/empty.dart';

class LoginView extends StatefulWidget {
  const LoginView({Key key}) : super(key: key);

  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  bool isWarm;
  ClockModel model;
  ClockBuilder clock;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Container(
        child: ListView(
          children: <Widget>[
            _images(
              double.infinity,
              500,
              'assets/animation/coding.flr',
              rootBundle,
              'coding',
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: _signInButton(),
            ),
//             ClockCustomizer(clock)
          ],
        ),
      ),
    );
  }

  Widget _signInButton() {
    return GestureDetector(
        onTap: () {
          signInWithGoogle().whenComplete(
            () {
              Navigator.of(context).pushNamed(RoutePaths.HomeScreen);
            },
          );
        },
        child: _images(
            400, 100,
            'assets/animation/pal.flr',
            rootBundle, 'Untitled',
        ),
    );
  }

  Widget _images(
    double width,
    double height,
    String name,
    AssetBundle bundle,
    String animation,
  ) {
    return SizedBox(
      width: width,
      height: height,
      child: FlareActor.asset(
        AssetFlare(bundle: bundle, name: name),
        alignment: Alignment.center,
        fit: BoxFit.contain,
        animation: animation,
      ),
    );
  }
}
